module ApplicationHelper
	def link_to_image(image_path, target_link, img_options={}, link_options={})
  		link_to(image_tag(image_path, img_options), target_link, link_options)
	end
end
