// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require jquery.validate
//= require jquery.validate.additional-methods
//= require jquery.blueimp-gallery.min
//= require bootstrap-image-gallery
//= require turbolinks
//= require bootstrap/
//= require tinymce
//= require_tree .

// Validation rules for bottom quote form
$(document).ready(function(){
	$('#quote-form').validate({
		errorClass: "has-error",
		errorPlacement: function(error, element) {
			error.appendTo(element.parent().parent());
		},
		highlight: function(element, errorClass, validClass) {
			$(element).parent().parent().addClass("has-error");
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parent().parent().removeClass("has-error");
		},
		rules: {
			name: "required",
			email: {
				required: true,
				email: true
			},
			phone: {
				required: true,
				phoneUS: true
			},
			request: "required"
		}
	});
});