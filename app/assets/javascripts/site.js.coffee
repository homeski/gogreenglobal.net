ready = ->
  $("#quote-form").on("ajax:success", (e, data, status, xhr) ->
    $("#quote-modal .modal-body").html "<p><strong>Success</strong></p><p>Thank you for your request. Someone from <brand>Go Green</brand> will contact you within 48 hours.</p><p><i>Name: #{data.json.name}</i></p><p><i>Email address: #{data.json.email}</i></p><p><i>Phone number: #{data.json.phone}</i></p><p><i>Request: #{data.json.request}</i></p>"
    $(@).find('.input-group input, textarea').each -> @.value = ""
  ).bind "ajax:error", (e, xhr, status, error) ->
    $("#quote-modal .modal-body").html "<p><strong>Error</strong></p><p>We're sorry but an error has occured. Please e-mail us directly at admin@gogreenglobal.net</p>"
  $("#quote-form .btn").click ->
  	$('#quote-modal').modal('toggle') if $("#quote-form").valid()
  	$("#quote-modal .modal-body").html "<p>Sending...</p>" if $("#quote-form").valid()

$(document).ready(ready)