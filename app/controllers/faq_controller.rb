class FaqController < ApplicationController
  def index
  	@page_title = "Frequently asked questions"
  	@meta_description = "Why Go Green? We're a company built on quality, integrity, and customer service. In order to properly landscape a property, you must be able to handle all aspects of that property's needs. Go Green's combination of great leadership and training allows us to provide for every aspect of your landscaping needs. Attention to detail will always set us apart from our competition"
  end
end
