class BlogController < ApplicationController

	def index
		@posts = Post.paginate(:page => params[:page], :per_page => 3).order('id DESC')

		@latest = Post.order('id DESC').limit(10)
	end

end
