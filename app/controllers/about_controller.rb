class AboutController < ApplicationController
  def index
  	@page_title = "The history of our company"
  	@meta_description = "In January of 2008 Go Green Landscape & Janitorial was formed with the hopes of creating a family owned company that would provide quality commercial services to Phoenix, Arizona. The company immediately began to take off under the leadership of Drue Engels in Janitorial and Kaipo Spenser heading up the Landscaping division. The family model is one that is still followed today, and has allowed Go Green to become a company with a winning attitude that provides unrivaled customer satisfaction."
  end
end
