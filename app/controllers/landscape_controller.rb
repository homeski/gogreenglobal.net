class LandscapeController < ApplicationController
  def index
  	@page_title = "Landscaping help we provide in Phoenix, AZ"
  	@meta_description = "Here at Go Green, we recognize that each property is going to be unique, with different objectives and challenges that need to be met. Landscape maintenance requires a lot of effort and hard work in order to achieve the goals set forth by the customer, and our teams unwavering attention to detail allows us to meet and exceed these goal"
  end
end
