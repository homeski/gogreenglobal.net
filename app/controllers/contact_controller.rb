include ERB::Util

class ContactController < ApplicationController
  def index
  	@page_title = "How to contact us in Phoenix, AZ"
  	@meta_description = "Where you can find us. Business Office 2331 East Alameda Drive Tempe, AZ 85254 Phone: 602-910-4220 Fax: 866-666-1364 Mailing Address 7119 East Shea Blvd Ste. 109 PMB 227 Tempe, AZ 85254"
  end

  # POST /
  def create
  	UserMailer.quote_email(params).deliver

  	params['name'] = html_escape(params['name'])
  	params['email'] = html_escape(params['email'])
  	params['phone'] = html_escape(params['phone'])
  	params['request'] = html_escape(params['request'])

  	render :json => { json: params, status: :success, status_code: "200" }
  end
end
