class JanitorialController < ApplicationController
  def index
  	@page_title = "Janitorial help we provide in Phoenix, AZ"
  	@meta_description = "Go Green believes in following a model that provides the highest quality janitorial services, and since most janitorial services are provided after business hours, you can remain confident that we only hire and keep the most honest and trustworthy employees to service your facilities."
  end
end
