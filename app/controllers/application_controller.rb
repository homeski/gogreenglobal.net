class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_site_head

  def set_site_head
  	@site_title = "Go Green Landscape & Janitorial Services"
    @meta_keywords = "Landscaping, Arborist, Contractor, Irrigation, Construction, Renovations, Janitorial, Custodial, Cleaning, Woman owned, Minority, Small business, Arizona, Scottsdale, Phoenix, Chandler, Mesa, Janitorial"
  end

  def index
  	@page_title = "Overview of what we can provide"
    @meta_description = "We're a family owned and operated full service commercial landscape management company caring for properties across the valley. We pride ourselves in maintaining all our properties to meet and exceed industry standards and always outperforming customer expectations. At Go Green customer satisfaction is our number one goal and the professional care we take with our properties allows us to achieve this."
  end
end
