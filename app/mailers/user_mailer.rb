class UserMailer < ActionMailer::Base
  default from: "webmaster@gogreenglobal.net"

  def quote_email(form)
  	@request = form[:request]
  	@email = form[:email]
  	@name = form[:name]
  	@phone = form[:phone]

	subjectLine = "Quote request from gogreenglobal.net"
	recipients = ["yourteam@gogreenglobal.net", "cody@gogreenglobal.net"]

  	mail(to: recipients, subject: subjectLine)
  end
end
